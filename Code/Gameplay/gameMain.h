
#pragma once


namespace JFF { namespace Gameplay
{
	void Initialize();

	void Update();

	void Render();

	void GameMain();
}}
