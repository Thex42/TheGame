
#include "Engine/main.h"
#include "gameMain.h"
#include "Engine/Core/Object.h"
#include "Engine/Core/Transform.h"
#include "Graphics/Render/Camera.h"
#include "Graphics/Render/MeshRenderer.h"
#include "Graphics/Render/RenderManager.h"
#include "Graphics/Lighting/LightDirectional.h"
#include "Engine/Shaders/Shader.h"


namespace JFF { namespace Gameplay
{
	JFFObject* cameraObject = nullptr;
	JFFCamera* cameraComponent = nullptr;

	const int BUNNIES_MODELS_COUNT = 3;
	const std::string BUNNY_TEXTURE_NAME = "Test.png";

	std::vector<JFFObject*> bunnies;

	JFFObject* lightObject1 = nullptr;
	JFFObject* lightObject2 = nullptr;
	JFFLightDirectional* light1 = nullptr;
	JFFLightDirectional* light2 = nullptr;



	void Initialize()
	{
		cameraObject = new JFFObject("MainCamera", float3(0.0f, 2.0f, 10.0f));
		cameraComponent = new JFFCamera(*cameraObject);
		cameraObject->AddComponent(cameraComponent);

		bunnies.reserve(BUNNIES_MODELS_COUNT);


		for (int i = 0; i < BUNNIES_MODELS_COUNT; ++i)
		{
			JFFObject* object = new JFFObject("Bunny", float3((i - (BUNNIES_MODELS_COUNT / 2)) * 2.0f, 0.0f, 0.0f));
			JFFMeshRenderer* meshRendererComponent = new JFFMeshRenderer(*object, "Bunny.obj", "unlit");
			meshRendererComponent->AssignTexture(BUNNY_TEXTURE_NAME);
			object->AddComponent(meshRendererComponent);
		
			bunnies.push_back(object);
		}


		lightObject1 = new JFFObject(float3::ZERO, float3(45.0f, -45.0f, 0.0f));
		light1 = new JFFLightDirectional(*lightObject1, 1.0f);
		lightObject2 = new JFFObject(float3::ZERO, -lightObject1->m_transform.GetEulerAngles());
		light2 = new JFFLightDirectional(*lightObject2, 0.2f);

		JFFRenderManager::Instance().SendLightToRenderItemsShaders(light1, 0);
		JFFRenderManager::Instance().SendLightToRenderItemsShaders(light2, 1);
	}

	void Update()
	{
		for (JFFObject* object : bunnies)
		{
			object->m_transform.Rotate(float3(0.0f, 1.0f, 0.0f) * 0.3333f);
		}

		//cameraObject->m_transform.Rotate(float3(0.0f, 1.0f, 0.0f) * 0.3333f);
	}

	void Render()
	{
		JFFRenderManager::SDL_Clear();
		JFFRenderManager::GL_Clear();

		JFFRenderManager::Instance().RenderSDLItems();
		JFFRenderManager::Instance().RenderGLItems(cameraComponent);
	}

	void GameMain()
	{
		SetApplicationCallbacks(Initialize, Update, Render);
	}
}}
