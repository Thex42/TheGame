
#pragma once

#include "Light.h"
#include "Engine/Types/float4.h"


namespace JFF
{
	class JFFLightDirectional : public JFFLight
	{
	public:

		JFFLightDirectional(JFFObject& object, float intensity = 1.0f);
		JFFLightDirectional(JFFObject& object, float4 const& color, float intensity = 1.0f);
		~JFFLightDirectional() final = default;

		std::string GetComponentName() const final { return "JFFLightDirectional"; }


		void SendLightToShader(JFFShader const* shader, int lightID) const final;
	};
}
