
#include "Light.h"


namespace JFF
{
	JFFLight::JFFLight(JFFObject& object, float intensity /*= 1.0f*/)
		: JFFComponent(object)
		, m_intensity(intensity)
	{

	}

	JFFLight::JFFLight(JFFObject& object, float4 const& color, float intensity /*= 1.0f*/)
		: JFFComponent(object)
		, m_color(color)
		, m_intensity(intensity)
	{

	}
}
