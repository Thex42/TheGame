
#include "LightDirectional.h"
#include "Engine/Core/Transform.h"
#include "Engine/Shaders/Shader.h"
#include <string>


namespace JFF
{
	JFFLightDirectional::JFFLightDirectional(JFFObject& object, float intensity /*= 1.0f*/)
		: JFFLight(object, intensity)
	{

	}

	JFFLightDirectional::JFFLightDirectional(JFFObject& object, float4 const& color, float intensity /*= 1.0f*/)
		: JFFLight(object, color, intensity)
	{

	}


	void JFFLightDirectional::SendLightToShader(JFFShader const* shader, int lightID) const
	{
		std::string lightName = "u_LightsDirectional[" + std::to_string(lightID) + "]";

		shader->SetVec3(-m_transform.GetForward(), lightName + ".m_direction");	// Need to invert direction for some reason
		shader->SetVec3(m_color.xyz(), lightName + ".m_color");
		shader->SetFloat(m_intensity, lightName + ".m_intensity");
	}
}
