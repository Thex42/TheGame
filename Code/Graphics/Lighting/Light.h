
#pragma once

#include "Engine/Core/Component.h"
#include "Engine/Types/float4.h"


namespace JFF
{
	class JFFShader;

	class JFFLight abstract : public JFFComponent
	{
	public:

		JFFLight(JFFObject& object, float intensity = 1.0f);
		JFFLight(JFFObject& object, float4 const& color, float intensity = 1.0f);
		~JFFLight() override = default;


		 virtual void SendLightToShader(JFFShader const* shader, int lightID) const = 0;


	protected:

		float4 m_color = float4::ONE;

		float m_intensity = 1.0f;
	};
}
