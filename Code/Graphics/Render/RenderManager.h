
#pragma once

#include <map>
#include <memory>
#include <vector>


namespace JFF
{
	class ISDLRenderItem;
	class JFFCamera;
	class JFFLight;
	class JFFMeshRenderer;
	class JFFShader;

	class JFFRenderManager
	{
		struct _privateConstructorAllowance_tag { explicit _privateConstructorAllowance_tag() = default; };

	public:

		static JFFRenderManager& Instance()
		{
			static std::unique_ptr<JFFRenderManager> s_instance = std::make_unique<JFFRenderManager>(_privateConstructorAllowance_tag{});
			return *s_instance;
		}

		JFFRenderManager(_privateConstructorAllowance_tag) {};
		~JFFRenderManager() = default;

		inline void RegisterRenderItem(ISDLRenderItem const* item) { m_sdlRenderItems.push_back(item); }
		inline void RegisterRenderItem(JFFMeshRenderer const* item) { m_glRenderItems.push_back(item); }
		void UnregisterRenderItem(ISDLRenderItem const* item);
		void UnregisterRenderItem(JFFMeshRenderer const* item);

		void RenderSDLItems() const;
		void RenderGLItems(JFFCamera const* camera) const;
		void SendLightToRenderItemsShaders(JFFLight const* light, int lightID) const;


		static void SDL_Clear();
		static void GL_Clear();


	private:

		std::vector<ISDLRenderItem const*> m_sdlRenderItems;
		std::vector<JFFMeshRenderer const*> m_glRenderItems;
	};
}
