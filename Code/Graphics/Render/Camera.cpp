
#include "Camera.h"
#include "Engine/Core/Object.h"
#include "Engine/Core/Transform.h"
#include "Engine/Shaders/Shader.h"
#include "Engine/Types/float3.h"
#include "Graphics/Render/MeshRenderer.h"
#include <gtc/matrix_transform.hpp>


namespace JFF
{
	JFFCamera::JFFCamera(JFFObject& object, float fov /*= 45.0f*/, float aspectRatio /*= Generics::SCREEN_SIZE_X / Generics::SCREEN_SIZE_Y*/, float nearPlane /*= 0.1f*/, float farPlane /*= 100.0f*/)
		: JFFComponent(object)
		, m_projectionMatrix()
		, m_fov(fov)
		, m_aspectRatio(aspectRatio)
		, m_nearPlane(nearPlane)
		, m_farPlane(farPlane)
	{
		UpdateProjectionMatrix();
		UpdateViewMatrix();

		m_transform.OnPositionChanged.AddObserver(this);
		m_transform.OnRotationChanged.AddObserver(this);
	}

	JFFCamera::~JFFCamera()
	{
		m_transform.OnPositionChanged.RemoveObserver(this);
		m_transform.OnRotationChanged.RemoveObserver(this);
	}

	void JFFCamera::RenderModel(JFFMeshRenderer const& meshRenderer) const
	{
		meshRenderer.UseShaderProgram();
		CalculateMVPForModel(meshRenderer);
		meshRenderer.Render();
	}

	void JFFCamera::UpdateProjectionMatrix()
	{
		m_projectionMatrix = glm::perspective(glm::radians(m_fov), m_aspectRatio, m_nearPlane, m_farPlane);
	}

	void JFFCamera::UpdateViewMatrix()
	{
		glm::vec3 cameraPosPos = m_transform.GetPos3D();
		m_viewMatrix = glm::lookAt(cameraPosPos, cameraPosPos + (glm::vec3)m_transform.GetForward(), (glm::vec3)m_transform.GetUp());
	}

	void JFFCamera::CalculateMVPForModel(JFFMeshRenderer const& meshRenderer) const
	{
		meshRenderer.SendMVPToShader(m_projectionMatrix, m_viewMatrix, meshRenderer.m_transform.GetModelMatrix());
	}

	void JFFCamera::ObservedEventUpdate(void* subject, std::string const& subjectName)
	{
		UpdateViewMatrix();
	}
}
