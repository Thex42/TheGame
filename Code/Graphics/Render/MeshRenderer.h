
#pragma once

#include "Engine/Core/Component.h"
#include <GL/glew.h>
#include <glm.hpp>
#include <string>
#include <vector>


namespace JFF
{
	class JFFMesh;
	class JFFObject;
	class JFFShader;
	class JFFTexture;

	class JFFMeshRenderer final : public JFFComponent
	{
	public:

		JFFMeshRenderer(JFFObject& object, std::string const& modelName = "", std::string const& shaderName = "", bool hasGeomShader = false);
		~JFFMeshRenderer() final;

		inline std::string GetComponentName() const final { return "JFFMeshRenderer"; }


		void AssignModel(std::string const& modelName);
		void AssignTexture(std::string const& texturePath);

		void UseShaderProgram() const;
		void SendMVPToShader(glm::mat4 const& modelMatrix, glm::mat4 const& viewMatrix, glm::mat4 const& projectionMatrix) const;
		void Render() const;

		inline JFFShader const* GetShader() const { return m_shader; }


	private:

		void GenAndBindVBOAndVAO();
		void BindVertices(std::vector<glm::vec3> const& vertices) const;
		void BindUVs(std::vector<glm::vec2> const& uvs) const;
		void BindNormalsIfExist(std::vector<glm::vec3> const& normals) const;


		JFFMesh const* m_mesh;
		JFFShader const* m_shader;
		JFFTexture const* m_texture;

		GLuint m_vbo[3];
		GLuint m_vaoID;
		GLuint m_textureID;

		// The positions of the position and color data within the VAO
		const uint32_t m_positionAttributeIndex;
		const uint32_t m_uvAttributeIndex;
		const uint32_t m_normalsAttributeIndex;
	};
}
