
#include "RenderManager.h"
#include "Camera.h"
#include "Engine/main.h"
#include "Engine/Shaders/Shader.h"
#include "Engine/Types/types.hpp"
#include "ISDLRenderItem.h"
#include "Graphics/Lighting/Light.h"
#include "Mesh.h"
#include "MeshRenderer.h"

constexpr int GL3_PROTOTYPES = 1;
#include <GL/glew.h>
#include <SDL.h>


namespace JFF
{
	void JFFRenderManager::UnregisterRenderItem(ISDLRenderItem const* item)
	{
		auto const& iterator = std::find(m_sdlRenderItems.begin(), m_sdlRenderItems.end(), item);

		m_sdlRenderItems[iterator - m_sdlRenderItems.begin()] = m_sdlRenderItems.back();	// Copy the last in here
		m_sdlRenderItems.pop_back();													// Then pop the last
	}

	void JFFRenderManager::UnregisterRenderItem(JFFMeshRenderer const* item)
	{
		auto const& iterator = std::find(m_glRenderItems.begin(), m_glRenderItems.end(), item);

		m_glRenderItems[iterator - m_glRenderItems.begin()] = m_glRenderItems.back();	// Copy the last in here
		m_glRenderItems.pop_back();												// Then pop the last
	}

	void JFFRenderManager::RenderSDLItems() const
	{
		for (auto const& renderItem : m_sdlRenderItems)
		{
			if (renderItem->GetRenderState() == ERenderState::Render)
				renderItem->Render();
		}
	}

	void JFFRenderManager::RenderGLItems(JFFCamera const* camera) const
	{
		for (auto const& model : m_glRenderItems)
			camera->RenderModel(*model);
	}

	void JFFRenderManager::SendLightToRenderItemsShaders(JFFLight const* light, int lightID) const
	{
		for (auto const& meshRenderer : m_glRenderItems)
		{
			JFFShader const* shader = meshRenderer->GetShader();
			shader->UseProgram();
			light->SendLightToShader(shader, lightID);
		}
	}

	void JFFRenderManager::SDL_Clear()
	{
		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		SDL_RenderClear(g_mainWindowRenderer);
	}

	void JFFRenderManager::GL_Clear()
	{
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	}
}
