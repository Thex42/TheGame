
#include "ISDLRenderItem.h"


namespace JFF
{
	ISDLRenderItem::ISDLRenderItem()
		: m_renderState(ERenderState::Render)
	{
		JFFRenderManager::Instance().RegisterRenderItem(this);
	}

	ISDLRenderItem::~ISDLRenderItem()
	{
		JFFRenderManager::Instance().UnregisterRenderItem(this);
	}
}
