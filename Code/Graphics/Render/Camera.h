
#pragma once

#include "Engine/Containers/EventListening.h"
#include "Engine/Core/Component.h"
#include "Engine/Helpers/generics.hpp"
#include <glm.hpp>
#include <string>


namespace JFF
{
	class JFFShader;
	class JFFObject;
	class JFFMeshRenderer;

	class JFFCamera final : public JFFComponent, public IEventObserver
	{
	public:

		JFFCamera(JFFObject& object, float fov = 45.0f, float aspectRatio = Generics::SCREEN_SIZE_X / Generics::SCREEN_SIZE_Y, float nearPlane = 0.1f, float farPlane = 100.0f);
		~JFFCamera() final;

		inline std::string GetComponentName() const final { return "JFFCamera"; }


		void RenderModel(JFFMeshRenderer const& model) const;

	private:

		void UpdateProjectionMatrix();
		void UpdateViewMatrix();
		void CalculateMVPForModel(JFFMeshRenderer const& model) const;

		void ObservedEventUpdate(void* subject, std::string const& subjectName) final;


		glm::mat4 m_projectionMatrix;
		glm::mat4 m_viewMatrix;

		float m_fov = 45.0f;
		float m_aspectRatio = Generics::SCREEN_SIZE_X / Generics::SCREEN_SIZE_Y;
		float m_nearPlane = 0.1f;
		float m_farPlane = 100.0f;
	};
}
