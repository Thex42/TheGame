
#include "Engine/Helpers/asserts.hpp"
#include "Mesh.h"
#include "Graphics/Images/Texture.h"
#include <fstream>
#include <string>

#define OUT


namespace JFF
{
	JFFMesh::JFFMesh(std::string const& modelFileName)
	{
		LoadFromOBJ(modelFileName);
	}

	bool JFFMesh::LoadFromOBJ(std::string const& modelFileName)
	{
		return ReadOBJ(modelFileName, OUT m_vertices, OUT m_uv, OUT m_normals);
	}

	bool JFFMesh::ReadOBJ(std::string const& modelFileName, std::vector<glm::vec3>& outVertices, std::vector<glm::vec2>&  outUVs, std::vector<glm::vec3>& outNormals)
	{
		std::ifstream file(modelFileName);

		Assert(file.is_open(), "Couldn't read file \"" + modelFileName + "\"");

		std::vector<unsigned int> vertexIndices, uvIndices, normalIndices;
		std::vector<glm::vec3> tempVertices;
		std::vector<glm::vec2> tempUVs;
		std::vector<glm::vec3> tempNormals;

		std::string facesFormat;
		AllocateVectors(modelFileName, OUT vertexIndices, OUT uvIndices, OUT normalIndices, OUT tempVertices, OUT tempUVs, OUT tempNormals, OUT facesFormat);

		bool hasUVs = facesFormat == "0/0" || facesFormat == "0/0/0";
		bool hasNormals = facesFormat == "0/0/0" || facesFormat == "0//0";

		std::string word;
		while (file >> word)
		{
			if (word == "v")
			{
				glm::vec3 vertex;
				file >> vertex.x >> vertex.y >> vertex.z;

				tempVertices.push_back(vertex);

				if (!hasUVs)
					tempUVs.emplace_back(vertex);	// Generate UVs from vertices if none are provided
			}

			else if (word == "vt")
			{
				glm::vec2 uv;
				file >> uv.x >> uv.y;

				tempUVs.push_back(uv);
			}

			else if (word == "vn")
			{
				glm::vec3 normal;
				file >> normal.x >> normal.y >> normal.z;

				tempNormals.push_back(normal);
			}

			else if (word == "f")
			{
				char valueSeparator = '/';
				unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];

				for (int i = 0; i < 3; ++i)
				{
					file >> vertexIndex[i];
					if (hasUVs)
						file >> valueSeparator >> uvIndex[i];
					else if (hasNormals)
						file >> valueSeparator;	// Should still get rid of a slash
					if (hasNormals)
						file >> valueSeparator >> normalIndex[i];
				}

				vertexIndices.push_back(vertexIndex[0]);
				vertexIndices.push_back(vertexIndex[1]);
				vertexIndices.push_back(vertexIndex[2]);

				if (hasUVs)
				{
					uvIndices.push_back(uvIndex[0]);
					uvIndices.push_back(uvIndex[1]);
					uvIndices.push_back(uvIndex[2]);
				}
				else
				{
					uvIndices.push_back(vertexIndex[0]);
					uvIndices.push_back(vertexIndex[1]);
					uvIndices.push_back(vertexIndex[2]);
				}

				if (hasNormals)
				{
					normalIndices.push_back(normalIndex[0]);
					normalIndices.push_back(normalIndex[1]);
					normalIndices.push_back(normalIndex[2]);
				}
			}

			else
			{
				file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			}
		}

		outVertices.reserve(tempVertices.size());
		for (auto const& vertexIndex : vertexIndices)
		{
			glm::vec3 vertex = tempVertices[vertexIndex - 1];
			outVertices.push_back(vertex);
		}

		outUVs.reserve(tempUVs.size());
		for (auto const& uvIndex : uvIndices)
		{
			glm::vec2 uv = tempUVs[uvIndex - 1];
			outUVs.push_back(uv);
		}

		if (hasNormals)
		{
			outNormals.reserve(tempNormals.size());
			for (auto const& normalIndex : normalIndices)
			{
				glm::vec3 normal = tempNormals[normalIndex - 1];
				outNormals.push_back(normal);
			}
		}

		file.close();

		return true;
	}

	void JFFMesh::AllocateVectors(std::string const& modelFilePath, std::vector<unsigned int>& outVertexIndices, std::vector<unsigned int>& outUVIndices, std::vector<unsigned int>&  outNormalIndices, std::vector<glm::vec3>& outVertices, std::vector<glm::vec2>& outUVs, std::vector<glm::vec3>& outNormals, std::string&  outFacesFormat)
	{
		std::ifstream file(modelFilePath);

		int verticesCount = 0;
		int uvCount = 0;
		int normalsCount = 0;
		int facesCount = 0;

		std::string word;
		while (file >> word)
		{
			if (word == "v")
			{
				++verticesCount;
			}

			else if (word == "vt")
			{
				++uvCount;
			}

			else if (word == "vn")
			{
				++normalsCount;
			}

			else if (word == "f")
			{
				++facesCount;

				if (outFacesFormat.empty())
				{
					bool previousCharWasNumber = false;
					file.get();	// Skip first space after "f"

					while (file.good())
					{
						char character = file.get();

						if (character == ' ')
						{
							file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');	// Skip the rest of the line
							break;
						}

						if (character == '/')
						{
							outFacesFormat.insert(outFacesFormat.end(), character);
							previousCharWasNumber = false;
						}
						else if (!previousCharWasNumber && isdigit(character))
						{
							outFacesFormat.insert(outFacesFormat.end(), '0');
							previousCharWasNumber = true;
						}
					}
				}
			}

			else
			{
				file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			}
		}

		outVertices.reserve(verticesCount);
		outUVs.reserve(uvCount);
		outNormals.reserve(normalsCount);

		outVertexIndices.reserve(facesCount);
		outUVIndices.reserve(facesCount);
		if (outFacesFormat == "0/0/0" || outFacesFormat == "0//0")
			outNormalIndices.reserve(facesCount);
	}


	std::map<std::string /*modelName*/, JFFMesh const*> JFFMeshesHolder::s_loadedMeshesByName;

	/*static*/ JFFMesh const* JFFMeshesHolder::GetOrCreateMesh(std::string const& modelFileName)
	{
		if (!HasMesh(modelFileName))
			s_loadedMeshesByName[modelFileName] = new JFFMesh("data/" + modelFileName);

		return s_loadedMeshesByName[modelFileName];
	}
}
