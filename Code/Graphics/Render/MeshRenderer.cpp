
#include "MeshRenderer.h"
#include "Mesh.h"
#include "RenderManager.h"
#include "Engine/Shaders/Shader.h"
#include "Graphics/Images/Texture.h"


namespace JFF
{
	JFFMeshRenderer::JFFMeshRenderer(JFFObject& object, std::string const& modelName /*= ""*/, std::string const& shaderName /*= ""*/, bool hasGeomShader /*= false*/)
		: JFFComponent(object)
		, m_mesh()
		, m_shader(JFFShadersHolder::GetOrCreateShader(shaderName, hasGeomShader))
		, m_textureID(0)
		, m_positionAttributeIndex(0)
		, m_uvAttributeIndex(1)
		, m_normalsAttributeIndex(2)
	{
		if (!modelName.empty())
			AssignModel(modelName);

		JFFRenderManager::Instance().RegisterRenderItem(this);
	}

	JFFMeshRenderer::~JFFMeshRenderer()
	{
		JFFRenderManager::Instance().UnregisterRenderItem(this);
	}


	void JFFMeshRenderer::AssignModel(std::string const& modelName)
	{
		m_mesh = JFFMeshesHolder::GetOrCreateMesh(modelName);
		if (m_mesh != nullptr)
			GenAndBindVBOAndVAO();
	}

	void JFFMeshRenderer::AssignTexture(std::string const& textureName)
	{
		m_texture = JFFTexturesHolder::GetOrCreateTexture(textureName);

		glGenTextures(1, &m_textureID);
		glBindTexture(GL_TEXTURE_2D, m_textureID);

		SDL_Surface const* const surface = m_texture->GetSurface();

		const int colorMode = surface->format->BytesPerPixel == 4 ? GL_RGBA : GL_RGB;

		glTexImage2D(GL_TEXTURE_2D, 0, colorMode, surface->w, surface->h, 0, colorMode, GL_UNSIGNED_BYTE, m_texture->GetPixelsDataAndLockSurface());

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glGenerateMipmap(GL_TEXTURE_2D);
	}

	void JFFMeshRenderer::UseShaderProgram() const
	{
		m_shader->UseProgram();
	}

	void JFFMeshRenderer::SendMVPToShader(glm::mat4 const& modelMatrix, glm::mat4 const& viewMatrix, glm::mat4 const& projectionMatrix) const
	{
		m_shader->SetMVP(modelMatrix, viewMatrix, projectionMatrix);
	}

	void JFFMeshRenderer::Render() const
	{
		glDrawArrays(GL_TRIANGLES, 0, m_mesh->VerticesCount());
	}

	void JFFMeshRenderer::GenAndBindVBOAndVAO()
	{
		glGenBuffers(3, m_vbo);
		glGenVertexArrays(1, &m_vaoID);

		glBindVertexArray(m_vaoID);


		BindVertices(m_mesh->GetVertices());
		BindUVs(m_mesh->GetUVs());
		BindNormalsIfExist(m_mesh->GetNormals());
	}

	void JFFMeshRenderer::BindVertices(std::vector<glm::vec3> const& vertices) const
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_vbo[0]);
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

		// Specify that our coordinate data is going into attribute index 0, and contains three floats per vertex
		const int valuesPerPosition = 3;
		glVertexAttribPointer(m_positionAttributeIndex, valuesPerPosition, GL_FLOAT, GL_FALSE, 0, nullptr);
		glEnableVertexAttribArray(m_positionAttributeIndex);
	}

	void JFFMeshRenderer::BindUVs(std::vector<glm::vec2> const& uvs) const
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_vbo[1]);
		glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);

		// Specify that our coordinate data is going into attribute index 0, and contains three floats per vertex
		const int valuesPerUV = 2;
		glVertexAttribPointer(m_uvAttributeIndex, valuesPerUV, GL_FLOAT, GL_FALSE, 0, nullptr);
		glEnableVertexAttribArray(m_uvAttributeIndex);
	}

	void JFFMeshRenderer::BindNormalsIfExist(std::vector<glm::vec3> const& normals) const
	{
		if (normals.empty())
			return;

		glBindBuffer(GL_ARRAY_BUFFER, m_vbo[2]);
		glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);

		// Specify that our coordinate data is going into attribute index 0, and contains three floats per vertex
		const int valuesPerNormal = 3;
		glVertexAttribPointer(m_normalsAttributeIndex, valuesPerNormal, GL_FLOAT, GL_FALSE, 0, nullptr);
		glEnableVertexAttribArray(m_normalsAttributeIndex);
	}
}
