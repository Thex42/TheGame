
#pragma once

#include <glm.hpp>
#include <map>
#include <memory>
#include <string>
#include <vector>


namespace JFF
{
	class JFFMeshesHolder;

	class JFFMesh
	{
		friend class JFFMeshesHolder;

	public:

		~JFFMesh() = default;


		bool LoadFromOBJ(std::string const& modelFileName);

		inline int VerticesCount() const { return (int)m_vertices.size(); }
		inline std::vector<glm::vec3> const& GetVertices() const { return m_vertices; }
		inline std::vector<glm::vec2> const& GetUVs() const { return m_uv; }
		inline std::vector<glm::vec3> const& GetNormals() const { return m_normals; }


	private:

		JFFMesh(std::string const& modelFileName);


		static bool ReadOBJ(std::string const& sPath, std::vector<glm::vec3>& outVertices, std::vector<glm::vec2>& outUVs, std::vector<glm::vec3>& outNormals);
		static void AllocateVectors(std::string const& sPath, std::vector<unsigned int>& outVertexIndices, std::vector<unsigned int>& outUVIndices, std::vector<unsigned int>& outNormalIndices, std::vector<glm::vec3>& outVertices, std::vector<glm::vec2>& outUVs, std::vector<glm::vec3>& outNormals, std::string& outFacesFormat);


		std::vector<glm::vec3> m_vertices;
		std::vector<glm::vec2> m_uv;
		std::vector<glm::vec3> m_normals;
	};


	class JFFMeshesHolder
	{
	public:

		static JFFMesh const* GetOrCreateMesh(std::string const& modelFileName);
		inline static JFFMesh const* GetMesh(std::string const& modelName) { return s_loadedMeshesByName[modelName]; }
		inline static bool HasMesh(std::string const& modelName) { return s_loadedMeshesByName.find(modelName) != s_loadedMeshesByName.end(); }


	private:

		JFFMeshesHolder() = default;


		static std::map<std::string /*modelName*/, JFFMesh const*> s_loadedMeshesByName;
	};
}
