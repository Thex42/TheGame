
#pragma once

#include "RenderManager.h"
#include <SDL.h>
#include <SDL_image.h>


namespace JFF
{
	enum class ERenderState
	{
		Render,
		ForceNoRender
	};

	class ISDLRenderItem
	{
		friend class JFFRenderManager;

	public:

		ISDLRenderItem();
		virtual ~ISDLRenderItem();

		void SetRenderState(ERenderState const&& renderState) { m_renderState = renderState; };
		const ERenderState& GetRenderState() const { return m_renderState; }


	private:

		virtual void Render(SDL_RendererFlip flipState = SDL_FLIP_NONE) const = 0;


		ERenderState m_renderState;
	};
}
