
#pragma once

#include "Engine/Types/float4.h"


namespace JFF
{
	struct float2;

	class JFFSquare
	{
	public:

		JFFSquare();
		JFFSquare(float2 const& pos, float2 const& dimensions);
		JFFSquare(float posX, float posY, float sizeX = 1.0f, float sizeY = 1.0f);
		~JFFSquare() = default;

		float2 const& Position() const { return m_data.xy(); }
		float2 const& Dimensions() const { return m_data.wz(); }

	private:

		float4 m_data;
	};
}
