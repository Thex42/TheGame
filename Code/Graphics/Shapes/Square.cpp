
#include "Square.h"
#include "Engine/Types/float2.h"


namespace JFF
{
	JFFSquare::JFFSquare()
		: m_data(0.0f, 0.0f, 1.0f, 1.0f)
	{

	}

	JFFSquare::JFFSquare(float2 const& pos, float2 const& dimensions)
		: m_data(pos, dimensions)
	{

	}

	JFFSquare::JFFSquare(float posX, float posY, float sizeX, float sizeY)
		: m_data(posX, posY, sizeX, sizeY)
	{

	}
}
