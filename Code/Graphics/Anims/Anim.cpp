
#include "Anim.h"
#include "Engine/Helpers/generics.hpp"
#include <SDL.h>
#include <SDL_image.h>


namespace JFF
{
	JFFAnim::JFFAnim(const char* textureName, const i32 framesPerSecond, const u32 frameSizeX, const u32 frameSizeY) :
		m_sprite(nullptr),
		m_spriteClips(nullptr),
		m_animSpeed(framesPerSecond),
		m_framesCount(0),
		m_currentFrame(0),
		m_lastFrameChange(0)
	{
		m_sprite = std::make_unique<JFFSprite>();

		m_sprite->LoadFromFile(textureName);

		m_framesCount = int(m_sprite->GetSizeX()) / frameSizeX;

		m_spriteClips = std::make_unique<SDL_Rect[]>(m_framesCount);

		for (u32 i = 0; i < m_framesCount; i++)
		{
			m_spriteClips[i].x = frameSizeX * i;
			m_spriteClips[i].y = 0;
			m_spriteClips[i].w = frameSizeX;
			m_spriteClips[i].h = frameSizeY;
		}

		m_sprite->SetCutout(m_spriteClips[0]);
	}


	void JFFAnim::Play()
	{
		if (m_lastFrameChange == 0)
		{
			m_lastFrameChange = SDL_GetTicks();
		}
		else if ((i32(SDL_GetTicks()) - i32(m_lastFrameChange)) >= (Generics::SECONDS / m_animSpeed))
		{
			m_currentFrame = (m_currentFrame + 1) % m_framesCount;

			m_sprite->SetCutout(m_spriteClips[m_currentFrame]);
			m_lastFrameChange = u32(SDL_GetTicks());
		}

		/*if (directionFacing == EDirection_Right)
		{
			GfxSpriteSetScale(m_sprite, 1.0f, 1.0f);
		}
		else
		{
			GfxSpriteSetScale(m_sprite, -1.0f, 1.0f);
		}*/
	}
}
