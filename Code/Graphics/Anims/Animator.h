
#pragma once

#include "Anim.h"
#include "Engine/Core/Component.h"


namespace JFF
{
	class JFFObject;

	class JFFAnimator : public JFFComponent
	{
	public:

		JFFAnimator(JFFObject& object);
		~JFFAnimator() final = default;

		std::string GetComponentName() const final { return "JFFAnimator"; }
	};
}
