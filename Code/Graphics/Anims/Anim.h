
#pragma once

#include "Engine/Types/types.hpp"
#include "Graphics/Images/Sprite.h"
#include <memory>


namespace JFF
{
	class JFFAnim
	{
	public:

		JFFAnim() = default;
		JFFAnim(const char* textureName, const i32 framesPerSecond, const u32 frameSizeX, const u32 frameSizeY);
		~JFFAnim() = default;

		void Play();

	private:

		std::unique_ptr<JFFSprite> m_sprite;
		std::unique_ptr<SDL_Rect[]> m_spriteClips;

		i32 m_animSpeed;		// Frames per second
		u32 m_framesCount;
		u32 m_currentFrame;
		u32 m_lastFrameChange;
	};
}
