
#pragma once

#include "Engine/Core/Object.h"
#include <memory>
#include <vector>


namespace JFF { namespace UI
{
	class JFFUIElement : public JFFObject
	{
	public:

		JFFUIElement();
		virtual ~JFFUIElement() = default;

	private:

		std::shared_ptr<JFFUIElement> m_parent;
		std::vector<std::unique_ptr<JFFUIElement>> m_children;
	};
}}
