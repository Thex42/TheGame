
#pragma once

#include "Engine/Types/types.hpp"
#include <map>
#include <SDL.h>
#include <string>


namespace JFF
{
	class JFFTexturesHolder;

	class JFFTexture
	{
		friend JFFTexturesHolder;

	public:

		~JFFTexture();


		bool LoadFromFile(std::string const& texturePath);
		void SetSurfaceLocked(bool locked) const;
		void Free();

		void SetColor(u32 r, u32 g, u32 b);
		void SetBlendMode(SDL_BlendMode blendMode);
		void SetAlpha(u32 a);

		inline SDL_Surface* GetSurface() const { return m_surface; }
		inline SDL_Texture* GetTexture() const { return m_texture; }
		void* GetPixelsDataAndLockSurface() const;


	private:

		JFFTexture(std::string const& texturePath);


		SDL_Surface* m_surface;
		SDL_Texture* m_texture;
	};


	class JFFTexturesHolder
	{
	public:

		static JFFTexture const* GetOrCreateTexture(std::string const& textureNameAndType);
		inline static JFFTexture const* GetTexture(std::string const& textureNameAndType) { return s_loadedTexturesByName[textureNameAndType]; }
		inline static bool HasTexture(std::string const& textureNameAndType) { return s_loadedTexturesByName.find(textureNameAndType) != s_loadedTexturesByName.end(); }


	private:

		JFFTexturesHolder() = default;


		static std::map<std::string /*textureNameAndType*/, JFFTexture const*> s_loadedTexturesByName;
	};
}
