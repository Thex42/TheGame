
#include "Sprite.h"
#include "Engine/Core/Transform.h"
#include "Engine/Helpers/asserts.hpp"
#include "Engine/main.h"
#include "Texture.h"
#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include <stdlib.h>


namespace JFF
{
	JFFSprite::JFFSprite()
		: JFFObject()
		, ISDLRenderItem()
		, m_cutout()
		, m_size()
		, m_texture(nullptr)
	{

	}

	JFFSprite::~JFFSprite()
	{
		delete m_texture;
	}


	bool JFFSprite::LoadFromFile(char const* textureName)
	{
		if (!m_texture->LoadFromFile(textureName))
			return false;

		SDL_Surface const* textureSurface = m_texture->GetSurface();

		m_size.x() = float(textureSurface->w);
		m_size.y() = float(textureSurface->h);

		return true;
	}

	void JFFSprite::Render(SDL_RendererFlip flipState) const
	{
		const float2 pos = m_transform.GetPos2D();
		SDL_Rect renderQuad = { int(pos.x()), int(pos.y()), int(m_cutout.w), int(m_cutout.h) };

		SDL_RenderCopyEx(g_mainWindowRenderer, m_texture->GetTexture(), &m_cutout, &renderQuad, 0.0f, nullptr, flipState);
	}

	void JFFSprite::SetColor(u32 r, u32 g, u32 b)
	{
		m_texture->SetColor(r, g, b);
	}

	void JFFSprite::SetBlendMode(SDL_BlendMode blendMode)
	{
		m_texture->SetBlendMode(blendMode);
	}

	void JFFSprite::SetAlpha(u32 a)
	{
		m_texture->SetAlpha(a);
	}

	void JFFSprite::SetCutout(const int x, const int y, const int w, const int h)
	{
		m_cutout.x = x;
		m_cutout.y = y;
		m_cutout.w = w;
		m_cutout.h = h;
	}

	void JFFSprite::SetCutout(const float2& pos, const float2& size)
	{
		m_cutout.x = int(pos.x());
		m_cutout.y = int(pos.y());
		m_cutout.w = int(size.x());
		m_cutout.h = int(size.y());
	}

	void JFFSprite::SetCutout(const SDL_Rect& rect)
	{
		m_cutout = rect;
	}

	void JFFSprite::ResetCutout()
	{
		m_cutout.x = 0;
		m_cutout.y = 0;
		m_cutout.w = int(m_size.x());
		m_cutout.h = int(m_size.y());
	}

	void JFFSprite::SetPos(const float x, const float y)
	{
		m_transform.SetPos(x, y);
	}

	void JFFSprite::SetPos(const float2& pos)
	{
		m_transform.SetPos(pos);
	}

	const float2& JFFSprite::GetSize() const
	{
		return m_size;
	}

	float JFFSprite::GetSizeX() const
	{
		return m_size.x();
	}

	float JFFSprite::GetSizeY() const
	{
		return m_size.y();
	}

	float2 JFFSprite::GetPos() const
	{
		return m_transform.GetPos2D();
	}

	float JFFSprite::GetPosX() const
	{
		return m_transform.GetPos2D().x();
	}

	float JFFSprite::GetPosY() const
	{
		return m_transform.GetPos2D().y();
	}
}
