
#include "Texture.h"
#include "Engine/Helpers/asserts.hpp"
#include "Engine/main.h"
#include <SDL_image.h>



namespace JFF
{
	JFFTexture::JFFTexture(std::string const& texturePath)
	{
		LoadFromFile(texturePath);
	}

	JFFTexture::~JFFTexture()
	{
		Free();
	}


	bool JFFTexture::LoadFromFile(std::string const& texturePath)
	{
		SDL_assert(m_texture == nullptr);
		Free();

		char errStr[255] = { 0 };


		m_surface = IMG_Load((texturePath).c_str());

		if (!m_surface)
		{
			snprintf(errStr, sizeof(errStr), "Unable to load image \"%s\"! SDL Error: %s\n", texturePath.c_str(), IMG_GetError());
			Assert(m_surface != nullptr, errStr);

			return false;
		}

		SDL_SetColorKey(m_surface, SDL_TRUE, SDL_MapRGB(m_surface->format, 0x00, 0xFF, 0xFF));

		m_texture = SDL_CreateTextureFromSurface(g_mainWindowRenderer, m_surface);

		if (m_texture == nullptr)
		{
			snprintf(errStr, sizeof(errStr), "Unable to create texture \"%s\" from surface! SDL Error: %s\n", texturePath.c_str(), SDL_GetError());
			Assert(m_texture != nullptr, errStr);

			return false;
		}

		SDL_SetSurfaceRLE(m_surface, true);

		return true;
	}

	void JFFTexture::SetSurfaceLocked(bool locked) const
	{
		if (locked)
			SDL_LockSurface(m_surface);
		else
			SDL_UnlockSurface(m_surface);
	}

	void JFFTexture::Free()
	{
		if (m_texture != nullptr)
		{
			SDL_FreeSurface(m_surface);
			SDL_DestroyTexture(m_texture);
			m_texture = nullptr;
		}
	}

	void JFFTexture::SetColor(u32 r, u32 g, u32 b)
	{
		SDL_SetTextureColorMod(m_texture, r, g, b);
	}

	void JFFTexture::SetBlendMode(SDL_BlendMode blendMode)
	{
		SDL_SetTextureBlendMode(m_texture, blendMode);
	}

	void JFFTexture::SetAlpha(u32 a)
	{
		SDL_SetTextureAlphaMod(m_texture, a);
	}

	void* JFFTexture::GetPixelsDataAndLockSurface() const
	{
		SetSurfaceLocked(true);
		return m_surface->pixels;
	}


	std::map<std::string /*textureNameAndType*/, JFFTexture const*> JFFTexturesHolder::s_loadedTexturesByName;

	/*static*/ JFFTexture const* JFFTexturesHolder::GetOrCreateTexture(std::string const& textureNameAndType)
	{
		if (!HasTexture(textureNameAndType))
			s_loadedTexturesByName[textureNameAndType] = new JFFTexture("data/" + textureNameAndType);

		return s_loadedTexturesByName[textureNameAndType];
	}
}
