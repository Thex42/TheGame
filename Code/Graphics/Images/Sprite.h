
#pragma once

#include "Engine/Core/Object.h"
#include "Engine/Types/float2.h"
#include "Graphics/Render/ISDLRenderItem.h"
#include "Engine/Types/types.hpp"


struct SDL_Texture;

namespace JFF
{
	class JFFTexture;


	class JFFSprite : public JFFObject, public ISDLRenderItem
	{
	public:

		JFFSprite();
		~JFFSprite() final;

		bool LoadFromFile(char const* textureName);

		void SetColor(u32 r, u32 g, u32 b);
		void SetBlendMode(SDL_BlendMode blendMode);
		void SetAlpha(u32 a);
		void SetCutout(const int x, const int y, const int w, const int h);
		void SetCutout(const float2& pos, const float2& size);
		void SetCutout(const SDL_Rect& rect);
		void ResetCutout();
		void SetPos(const float x, const float y);
		void SetPos(const float2& pos);

		const float2& GetSize() const;
		float GetSizeX() const;
		float GetSizeY() const;
		float2 GetPos() const;
		float GetPosX() const;
		float GetPosY() const;

	private:

		void Render(SDL_RendererFlip flipState = SDL_FLIP_NONE) const final;

		SDL_Rect m_cutout;
		float2 m_size;

		JFFTexture* m_texture;
	};
}
