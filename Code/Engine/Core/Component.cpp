
#include "Component.h"
#include "Object.h"
#include "Transform.h"


namespace JFF
{
	JFFComponent::JFFComponent(JFFObject& object)
		: m_object(object)
		, m_transform(object.m_transform)
	{

	}
}
