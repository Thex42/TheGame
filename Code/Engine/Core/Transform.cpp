
#include "Transform.h"
#include "Object.h"
#include <gtc/matrix_transform.hpp>


namespace JFF
{
	JFFTransform::JFFTransform(JFFObject& object, JFFTransform* parent /*= nullptr*/)
		: JFFComponent(object)
		, m_parent(parent)
	{
		UpdateDirections();
		UpdateModelMatrix();
	}

	JFFTransform::JFFTransform(JFFObject& object, float3 const& pos, JFFTransform* parent /*= nullptr*/)
		: JFFComponent(object)
		, m_parent(parent)
		, m_pos(pos)
	{
		UpdateDirections();
		UpdateModelMatrix();
	}

	JFFTransform::JFFTransform(JFFObject& object, float3 const& pos, float3 const& eulerAngles, JFFTransform* parent /* = nullptr*/)
		: JFFComponent(object)
		, m_parent(parent)
		, m_rotation(eulerAngles)
		, m_pos(pos)
	{
		UpdateDirections();
		UpdateModelMatrix();
	}

	void JFFTransform::SetPos(float2 const& newPos)
	{
		m_pos = newPos;
		UpdateModelMatrix();

		OnPositionChanged(&m_pos, "m_pos");
	}

	void JFFTransform::SetPos(float3 const& newPos)
	{
		m_pos = newPos;
		UpdateModelMatrix();

		OnPositionChanged(&m_pos, "m_pos");
	}

	void JFFTransform::SetPos(float x, float y, float z)
	{
		m_pos.Set(x, y, z);
		UpdateModelMatrix();

		OnPositionChanged(&m_pos, "m_pos");
	}

	float3 JFFTransform::GetEulerAngles() const
	{
		return glm::degrees(glm::eulerAngles(m_rotation));
	}

	void JFFTransform::SetEulerAngles(float3 const& newEulerAngles)
	{
		m_rotation = glm::quat(glm::radians((glm::vec3)newEulerAngles));
		UpdateDirections();
		UpdateModelMatrix();

		OnRotationChanged(&m_rotation, "m_rotation");
	}

	void JFFTransform::SetEulerAngles(float x, float y, float z)
	{
		SetEulerAngles(float3(x, y, z));
	}

	void JFFTransform::Rotate(float3 const& rotation)
	{
		m_rotation = glm::quat(glm::radians((glm::vec3)rotation)) * m_rotation;
		UpdateDirections();
		UpdateModelMatrix();

		OnRotationChanged(&m_rotation, "m_rotation");
	}

	void JFFTransform::SetScale(float3 const& newScale)
	{
		m_scale = newScale;
		UpdateModelMatrix();

		OnScaleChanged(&m_scale, "m_scale");
	}

	void JFFTransform::SetScale(float x, float y, float z)
	{
		m_scale.Set(x, y, z);
		UpdateModelMatrix();

		OnScaleChanged(&m_scale, "m_scale");
	}

	void JFFTransform::UpdateModelMatrix()
	{
		glm::mat4 translation = glm::translate(glm::mat4(1.0f), (glm::vec3)m_pos);
		glm::mat4 rotation = glm::mat4_cast(m_rotation);
		glm::mat4 scale = glm::scale(glm::mat4(1.0f), (glm::vec3)m_scale);

		m_modelMatrix = translation * (rotation * scale);
	}

	void JFFTransform::UpdateDirections()
	{
		m_forward = float3(m_rotation * glm::vec4((glm::vec3)float3::FORWARD, 0.0f));
		m_right = float3(m_rotation * glm::vec4((glm::vec3)float3::RIGHT, 0.0f));
		m_up = float3(m_rotation * glm::vec4((glm::vec3)float3::UP, 0.0f));
	}
}
