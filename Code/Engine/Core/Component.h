
#pragma once

#include <string>


namespace JFF
{
	class JFFObject;
	class JFFTransform;

	class JFFComponent
	{
	public:

		JFFComponent(JFFObject& object);
		virtual ~JFFComponent() = default;

		virtual std::string GetComponentName() const = 0;


		JFFObject& m_object;
		JFFTransform& m_transform;
	};
}
