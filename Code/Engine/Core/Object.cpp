
#include "Object.h"


namespace JFF
{
	JFFObject::JFFObject(JFFTransform* parent /*= nullptr*/)
		: m_transform(*this, parent)
	{

	}

	JFFObject::JFFObject(std::string const& name, JFFTransform* parent /*= nullptr*/)
		: m_transform(*this, parent)
		, m_name(name)
	{

	}

	JFFObject::JFFObject(float3 pos, JFFTransform* parent /*= nullptr*/)
		: m_transform(*this, pos, parent)
	{

	}

	JFFObject::JFFObject(std::string const& name, float3 pos, JFFTransform* parent /*= nullptr*/)
		: m_transform(*this, pos, parent)
		, m_name(name)
	{

	}

	JFFObject::JFFObject(float3 pos, float3 eulerAngles, JFFTransform* parent /*= nullptr*/)
		: m_transform(*this, pos, eulerAngles, parent)
	{

	}

	JFFObject::JFFObject(std::string const& name, float3 pos, float3 eulerAngles, JFFTransform* parent /*= nullptr*/)
		: m_transform(*this, pos, eulerAngles, parent)
		, m_name(name)
	{

	}
}
