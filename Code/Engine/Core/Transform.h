
#pragma once

#include "Engine/Containers/EventListening.h"
#include "Engine/Types/float3.h"
#include "Component.h"
#include <gtc/quaternion.hpp>
#include <string>


namespace JFF
{
	class JFFObject;

	class JFFTransform : public JFFComponent
	{
	public:

		JFFTransform(JFFObject& object, JFFTransform* parent = nullptr);
		JFFTransform(JFFObject& object, float3 const& pos, JFFTransform* parent = nullptr);
		JFFTransform(JFFObject& object, float3 const& pos, float3 const& eulerAngles, JFFTransform* parent = nullptr);

		~JFFTransform() final = default;


		inline std::string GetComponentName() const final { return "JFFTransform"; }


		inline glm::mat4 GetModelMatrix() const { return m_modelMatrix; }

		inline float2 GetPos2D() const { return m_pos.xy(); }
		inline const float3& GetPos3D() const { return m_pos; }
		void SetPos(float2 const& newPos);
		void SetPos(float3 const& newPos);
		void SetPos(float x, float y, float z = 0.0f);

		inline void Translate(float3 const& translation) { SetPos(m_pos + translation); }

		// In degrees
		float3 GetEulerAngles() const;
		// In degrees
		void SetEulerAngles(float3 const& newEulerAngles);
		// In degrees
		void SetEulerAngles(float x, float y, float z);

		// Rotation is in degrees
		void Rotate(float3 const& rotation);

		inline float3 GetForward() const { return m_forward; }
		inline float3 GetUp() const { return m_up; }
		inline float3 GetRight() const { return m_right; }

		inline float2 GetScale2D() const { return m_scale.xy(); }
		inline float3 const& GetScale3D() const { return m_scale; }
		void SetScale(float3 const& newScale);
		void SetScale(float x, float y, float z);

		inline JFFTransform* GetParent() { return m_parent; }
		inline JFFTransform const* GetParent() const { return m_parent; }
		inline void SetParent(JFFTransform* parent) { m_parent = parent; }


		EventSubject OnPositionChanged;
		EventSubject OnRotationChanged;
		EventSubject OnScaleChanged;


	protected:

		JFFTransform* m_parent;

		glm::quat m_rotation = glm::vec3(0.0f);	// Default quat isn't identity, need to construct it from zero euler
		float3 m_pos;
		float3 m_scale = float3::ONE;

		float3 m_forward;
		float3 m_up;
		float3 m_right;


	private:

		void UpdateModelMatrix();
		void UpdateDirections();

		glm::mat4 m_modelMatrix;
	};
}
