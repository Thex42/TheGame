
#pragma once

#include "Engine/Types/float3.h"
#include "Component.h"
#include "Transform.h"
#include <map>
#include <memory>
#include <string>


namespace JFF
{
	class JFFTransform;

	class JFFObject
	{
	public:

		JFFObject(JFFTransform* parent = nullptr);
		JFFObject(std::string const& name, JFFTransform* parent = nullptr);

		JFFObject(float3 pos, JFFTransform* parent = nullptr);
		JFFObject(std::string const& name, float3 pos, JFFTransform* parent = nullptr);

		JFFObject(float3 pos, float3 eulerAngles, JFFTransform* parent = nullptr);
		JFFObject(std::string const& name, float3 pos, float3 eulerAngles, JFFTransform* parent = nullptr);

		virtual ~JFFObject() = default;


		inline void AddComponent(JFFComponent* component) { m_components[component->GetComponentName()] = std::unique_ptr<JFFComponent>(component); }
		inline JFFComponent* GetComponent(std::string componentType) { return m_components[componentType].get(); }


		JFFTransform m_transform;


	private:

		std::string m_name = "Object";

		std::map<const std::string, std::unique_ptr<JFFComponent>> m_components;
	};


	static const JFFObject WORLD_ORIGIN;
}
