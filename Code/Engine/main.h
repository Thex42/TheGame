
#pragma once

#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>


extern SDL_Window* g_mainWindow;
extern SDL_Renderer* g_mainWindowRenderer;
extern SDL_GLContext g_mainWindowContext;

namespace JFF
{
	bool Init(SDL_Window** mainWindow, SDL_GLContext* mainWindowContext, SDL_Renderer** mainWindowRenderer);
	bool CreateWindow(SDL_Window** window, SDL_GLContext* context, SDL_Renderer** renderer, SDL_Surface** screenSurface = nullptr, const char* windowName = "It's a window.");
	void InitGlew();
	bool LoadBMPImage(SDL_Surface** surface, SDL_Surface* windowSurface, const char* imageName);
	bool LoadPNGImage(SDL_Surface** surface, SDL_Surface* windowSurface, const char* imageName);
	void CloseWindow(SDL_Window** window, SDL_GLContext* context, SDL_Renderer** renderer, SDL_Surface** screenSurface = nullptr);
	void CloseImage(SDL_Surface** surface);

	using VoidCallback = void(*)();
	void SetApplicationCallbacks(VoidCallback initialize, VoidCallback update, VoidCallback render);
}
