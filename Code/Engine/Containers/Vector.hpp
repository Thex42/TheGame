
#pragma once

#include "Engine/Helpers/Asserts.hpp"
#include "Engine/Helpers/generics.hpp"
#include "Engine/Types/types.hpp"
#include <cstddef>
#include <initializer_list>
#include <memory>


namespace JFF
{
	template<class T>
	class JFFVector
	{
		using iterator = T * ;

	public:
		JFFVector(std::size_t capacity = 1);
		JFFVector(JFFVector const& vector);
		JFFVector(std::initializer_list<T> list);
		~JFFVector() = default;

		inline T& operator[](std::size_t index) { return m_data[u32(index) + m_dataOffset]; }
		inline T const& operator[](std::size_t index) const { return m_data[u32(index) + m_dataOffset]; }
		void operator = (T const* const list);

		T Pop();
		std::size_t Find(T const& item) const;

		void Reserve(std::size_t capacity);
		void PushBack(T item);
		void PushBack_AssumeSufficientCapacity(T item);
		void PushBack_AssumeSufficientCapacityNoAssert(T item);
		void PushBack_AssumeSufficientCapacityNoRepackNoAssert(T item);

		bool Remove(T const& item);
		bool RemoveAt(u32 index);
		bool Remove_AssertWasInside(T const& item);
		void Clear();

		inline std::size_t Size() const { return m_size; }
		inline bool IsEmpty() const { return m_size > 0; }
		bool Contains(T const& item) const;

		inline iterator Begin() { return &(m_data[m_dataOffset]); }
		inline iterator End() { return &(m_data[m_dataOffset + m_size - 1]); }
		inline const iterator Begin() const { return &(m_data[m_dataOffset]); }
		inline const iterator End() const { return &(m_data[m_dataOffset + m_size - 1]); }

	private:

		void Expand();
		void Repack();

		inline u32 TrueEnd() const { return m_dataOffset + u32(m_size); }
		inline u32 CapacityAtEnd() const { return u32(m_capacity) - TrueEnd(); }

		std::unique_ptr<T[]> m_data;
		std::size_t m_size;
		std::size_t m_capacity;
		u32 m_dataOffset;
	};


	//-----------------------	DEFINITIONS	  -----------------------//

	template<class T>
	JFFVector<T>::JFFVector(std::size_t capacity)
		: m_data(nullptr)
		, m_size(0)
		, m_capacity(capacity)
		, m_dataOffset(0)
	{
		if (capacity <= 0)
			capacity = 1;
		m_data = std::unique_ptr<T[]>(Generics::CreateBuffer<T>(capacity));
	}

	template<class T>
	JFFVector<T>::JFFVector(JFFVector const& vector)
		: JFFVector(vector.size())
	{
		for (u32 i = 0; i < m_size; ++i)
			this->PushBack_AssumeSufficientCapacityNoAssert(vector[i]);
	}

	template<class T>
	JFFVector<T>::JFFVector(std::initializer_list<T> list)
		: JFFVector(std::size(list))
	{
		for (const T item : list)
			this->PushBack_AssumeSufficientCapacityNoAssert(item);
	}

	template<class T>
	void JFFVector<T>::operator = (T const* const list)
	{
		Clear();
		const u32 listSize = sizeof(*list) / sizeof(T);

		for (u32 i = 0; i < listSize; ++i)
			PushBack(list[i]);
	}

	template<class T>
	T JFFVector<T>::Pop()
	{
		T item = (*this)[0];
		++m_dataOffset;
		--m_size;
		return item;
	}

	template<class T>
	std::size_t JFFVector<T>::Find(T const& item) const
	{
		for (u32 i = 0; i < m_size; ++i)
		{
			if ((*this)[i] == item)
				return i;
		}

		return -1;
	}

	template<class T>
	void JFFVector<T>::Reserve(std::size_t requestedCapacity)
	{
		if (m_dataOffset != 0 && CapacityAtEnd() < requestedCapacity)
			Repack();

		const std::size_t newCapacity = requestedCapacity - (m_capacity - m_size);

		if (newCapacity <= 0)
			return;

		m_capacity += newCapacity;

		T* newData = nullptr;
		newData = Generics::CreateBuffer<T>(m_capacity);

		for (u32 i = 0; i < m_size; ++i)
		{
			newData[i] = (*this)[i];
			(*this)[i] = 0;
		}

		m_data.reset(newData);
	}

	template<class T>
	void JFFVector<T>::PushBack(T item)
	{
		if ((m_size + 1) + m_dataOffset >= m_capacity)
		{
			if (m_dataOffset != 0)
				Repack();
			else
				Expand();
		}

		(*this)[u32(m_size)] = item;
		++m_size;
	}

	template<class T>
	void JFFVector<T>::PushBack_AssumeSufficientCapacity(T item)
	{
		Assert(m_size + 1 <= m_capacity, "AssumeSufficientCapacity call where clearly it was not sufficient.");

		if (m_dataOffset != 0 && CapacityAtEnd() < 1)
			Repack();

		(*this)[u32(m_size)] = item;
		++m_size;
	}

	template<class T>
	void JFFVector<T>::PushBack_AssumeSufficientCapacityNoAssert(T item)
	{
		if (m_dataOffset != 0 && CapacityAtEnd() < 1)
			Repack();

		(*this)[u32(m_size)] = item;
		++m_size;
	}

	template<class T>
	void JFFVector<T>::PushBack_AssumeSufficientCapacityNoRepackNoAssert(T item)
	{
		(*this)[u32(m_size)] = item;
		++m_size;
	}

	template<class T>
	bool JFFVector<T>::Remove(T const& item)
	{
		bool itemGotRemoved = false;

		for (u32 i = 0; i < m_size; ++i)
		{
			if (itemGotRemoved)
			{
				(*this)[i] = (*this)[i + 1];
			}
			else if ((*this)[i] == item)
			{
				itemGotRemoved = true;
				--m_size;
			}
		}

		return itemGotRemoved;
	}

	template<class T>
	bool JFFVector<T>::RemoveAt(u32 index)
	{
		if (index < m_size)
			--m_size;
		else
			return false;

		for (u32 i = index; i < m_size; ++i)
			(*this)[i] = (*this)[i + 1];

		return true;
	}

	template<class T>
	bool JFFVector<T>::Remove_AssertWasInside(T const& item)
	{
		bool itemGotRemoved = false;

		for (u32 i = 0; i < m_size; ++i)
		{
			if (itemGotRemoved)
				(*this)[i] = (*this)[i + 1];
			else if ((*this)[i] == item)
				itemGotRemoved = true;
		}

		Assert(itemGotRemoved, "Item was not in vector.");
		return itemGotRemoved;
	}

	template<class T>
	void JFFVector<T>::Clear()
	{
		m_dataOffset = 0;
		m_size = 0;
	}

	template<class T>
	void JFFVector<T>::Expand()
	{
		Reserve(m_capacity * 2);
	}

	template<class T>
	void JFFVector<T>::Repack()
	{
		for (u32 i = 0; i < m_size; ++i)
			m_data[i] = (*this)[i];

		m_dataOffset = 0;
	}

	template<class T>
	bool JFFVector<T>::Contains(T const& item) const
	{
		for (u32 i = 0; i < m_size; ++i)
		{
			if ((*this)[i] == item)
				return true;
		}

		return false;
	}
}
