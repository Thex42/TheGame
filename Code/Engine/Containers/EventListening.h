
#pragma once

#include <string>
#include <vector>


namespace JFF
{
	class IEventObserver
	{
		friend class EventSubject;

	protected:

		virtual void ObservedEventUpdate(void* subject, std::string const& subjectName) = 0;
	};


	// Class that serves as a trigger for event listener pattern // Todo: improve to remove unsafe void* and string
	class EventSubject
	{
	public:

		EventSubject() = default;
		~EventSubject() = default;

		void Invoke(void* subject = nullptr, std::string const& subjectName = nullptr)
		{
			for (auto& listener : m_listeners)
				listener->ObservedEventUpdate(subject, subjectName);
		}

		inline void AddObserver(IEventObserver* observer) { m_listeners.push_back(observer); }
		void RemoveObserver(IEventObserver* observer);
		inline void operator () (void* subject = nullptr, std::string const& subjectName = nullptr) { Invoke(subject, subjectName); }

	private:

		std::vector<IEventObserver*> m_listeners;
	};
}
