
#include "EventListening.h"


void JFF::EventSubject::RemoveObserver(IEventObserver* observer)
{
	auto const& iterator = std::find(m_listeners.begin(), m_listeners.end(), observer);

	m_listeners[iterator - m_listeners.begin()] = m_listeners.back();	// Copy the last in here
	m_listeners.pop_back();										// Then pop the last
}
