
#define SDL_MAIN_HANDLED

#include "main.h"
#include "Gameplay/gameMain.h"
#include "Helpers/generics.hpp"

#include <GL/glew.h>
#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>


SDL_Window* g_mainWindow = nullptr;
SDL_Renderer* g_mainWindowRenderer = nullptr;
SDL_GLContext g_mainWindowContext = nullptr;

JFF::VoidCallback g_initCallback = nullptr;
JFF::VoidCallback g_updateCallback = nullptr;
JFF::VoidCallback g_pRenderCallback = nullptr;


int main(int argc, char* args[])
{
	if (!JFF::Init(&g_mainWindow, &g_mainWindowContext, &g_mainWindowRenderer))
		return 1;

	JFF::Gameplay::GameMain();

	g_initCallback();

	bool shouldQuit = false;
	SDL_Event sdlEvents;

	while (!shouldQuit)
	{
		while (SDL_PollEvent(&sdlEvents))
		{
			shouldQuit = (sdlEvents.type == SDL_QUIT);

			if (sdlEvents.type == SDL_KEYDOWN)
			{
				/*switch (sdlEvents.key.keysym.sym)
				{
				case SDLK_UP:

					t_g_mainCamera->m_transform.Translate(JFF::float3(0.0f, 0.3333f, 0.0f));
					break;

				case SDLK_DOWN:

					t_g_mainCamera->m_transform.Translate(JFF::float3(0.0f, -0.3333f, 0.0f));
					break;

				case SDLK_LEFT:

					t_g_mainCamera->m_transform.Translate(JFF::float3(-0.3333f, 0.0f, 0.0f));
					break;

				case SDLK_RIGHT:

					t_g_mainCamera->m_transform.Translate(JFF::float3(0.3333f, 0.0f, 0.0f));
					break;

				case SDLK_w:

					t_g_mainCamera->m_transform.Translate(JFF::float3(0.0f, 0.0f, 0.3333f));
					break;

				case SDLK_s:

					t_g_mainCamera->m_transform.Translate(JFF::float3(0.0f, 0.0f, -0.3333f));
					break;

				default:
					break;
				}*/
			}
		}

		g_updateCallback();

		g_pRenderCallback();
		SDL_GL_SwapWindow(g_mainWindow);
	}

	JFF::CloseWindow(&g_mainWindow, &g_mainWindowContext, &g_mainWindowRenderer);

	IMG_Quit();
	SDL_Quit();

	return 0;
}


namespace JFF
{
	void SetApplicationCallbacks(VoidCallback initialize, VoidCallback update, VoidCallback render)
	{
		g_initCallback = initialize;
		g_updateCallback = update;
		g_pRenderCallback = render;
	}


	bool Init(SDL_Window** mainWindow, SDL_GLContext* mainWindowContext, SDL_Renderer** mainWindowRenderer)
	{
		if (SDL_Init(SDL_INIT_VIDEO) != 0)
		{
			printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
			return false;
		}

		if (!CreateWindow(mainWindow, mainWindowContext, mainWindowRenderer))
		{
			return false;
		}

		InitGlew();

		int imgFlags = IMG_INIT_PNG;

		if (!(IMG_Init(imgFlags) & imgFlags))
		{
			printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
			return false;
		}

		return true;
	}

	bool CreateWindow(SDL_Window** window, SDL_GLContext* context, SDL_Renderer** renderer, SDL_Surface** screenSurface, const char* windowName)
	{
		*window = SDL_CreateWindow(windowName, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, Generics::SCREEN_SIZE_X, Generics::SCREEN_SIZE_Y, SDL_WINDOW_OPENGL);

		if (!(*window))
		{
			printf("Window \"%s\" could not be created! SDL_Error: %s\n", windowName, SDL_GetError());
			return false;
		}

		*context = SDL_GL_CreateContext(*window);

		if (!context)
		{
			printf("Context of window \"%s\" could not be created! SDL_Error: %s\n", windowName, SDL_GetError());
			return false;
		}

		*renderer = SDL_CreateRenderer(*window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

		if (!(*renderer))
		{
			printf("Renderer of window \"%s\" could not be created! SDL_Error: %s\n", windowName, SDL_GetError());
			return false;
		}

		SDL_SetRenderDrawColor(*renderer, 0xFF, 0xFF, 0xFF, 0xFF);

		if (screenSurface)
			*screenSurface = SDL_GetWindowSurface(*window);

		return true;
	}

	void InitGlew()
	{
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

		SDL_GL_SetSwapInterval(1);

		glewExperimental = GL_TRUE;
		glewInit();

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
	}

	bool LoadBMPImage(SDL_Surface** surface, SDL_Surface* windowSurface, const char* imageName)
	{
		SDL_Surface* rawImage = SDL_LoadBMP(imageName);

		if (!rawImage)
		{
			printf("Unable to load image \"%s\"! SDL Error: %s\n", imageName, SDL_GetError());

			return false;
		}

		*surface = SDL_ConvertSurface(rawImage, windowSurface->format, NULL);

		if (!(*surface))
		{
			printf("Unable to convert image \"%s\"! SDL Error: %s\n", imageName, SDL_GetError());

			return false;
		}

		SDL_FreeSurface(rawImage);

		return true;
	}

	bool LoadPNGImage(SDL_Surface** surface, SDL_Surface* windowSurface, const char* imageName)
	{
		SDL_Surface* rawImage = IMG_Load(imageName);

		if (!rawImage)
		{
			printf("Unable to load image \"%s\"! SDL Error: %s\n", imageName, IMG_GetError());

			return false;
		}

		*surface = SDL_ConvertSurface(rawImage, windowSurface->format, NULL);

		if (!(*surface))
		{
			printf("Unable to convert image \"%s\"! SDL Error: %s\n", imageName, SDL_GetError());

			return false;
		}

		SDL_FreeSurface(rawImage);

		return true;
	}

	void CloseWindow(SDL_Window** window, SDL_GLContext* context, SDL_Renderer** renderer, SDL_Surface** screenSurface)
	{
		SDL_GL_DeleteContext(*context);

		SDL_DestroyRenderer(*renderer);
		*renderer = nullptr;

		if (screenSurface)
			*screenSurface = nullptr;

		SDL_DestroyWindow(*window);
		*window = nullptr;
	}

	void CloseImage(SDL_Surface** surface)
	{
		SDL_FreeSurface(*surface);
		*surface = nullptr;
	}
}
