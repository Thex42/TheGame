
#include "float3.h"
#include "float4.h"
#include "Engine/Helpers/Asserts.hpp"
#include <float.h>
#include <math.h>


namespace JFF
{
	const float3 float3::INVALID = float3(FLT_MAX, FLT_MAX, FLT_MAX);

	const float3 float3::ZERO = float3(0.0f, 0.0f, 0.0f);
	const float3 float3::ONE = float3(1.0f, 1.0f, 1.0f);
	const float3 float3::FORWARD = float3(0.0f, 0.0f, -1.0f);
	const float3 float3::RIGHT = float3(1.0f, 0.0f, 0.0f);
	const float3 float3::UP = float3(0.0f, 1.0f, 0.0f);


	float3::float3(float x, float y, float z)
		: m_x(x)
		, m_y(y)
		, m_z(z)
	{

	}

	float3::float3(float2 const& xy, float z)
		: m_x(xy.x())
		, m_y(xy.y())
		, m_z(z)
	{

	}

	float3::float3(glm::vec3 const& xyz)
		: m_x(xyz.x)
		, m_y(xyz.y)
		, m_z(xyz.z)
	{

	}


	void float3::Set(float x, float y, float z)
	{
		m_x = x;
		m_y = y;
		m_z = z;
	}

	float float3::DotProduct(float3 const& vec) const
	{
		return (m_x * vec.m_x) + (m_y * vec.m_y) + (m_z * vec.m_z);
	}

	float float3::Length() const
	{
		return sqrtf(SqrdLength());
	}

	float float3::SqrdLength() const
	{
		return DotProduct(*this);
	}

	float3 float3::Normalize() const
	{
		const float lenght = Length();
		Assert(lenght > 0.0f, "Trying to normalize a vector of size 0");
		return ((*this) / lenght);
	}


	float3 float3::operator-()
	{
		return float3(-m_x, -m_y, -m_z);
	}

	void float3::operator += (float3 const& vec)
	{
		m_x += vec.m_x;
		m_y += vec.m_y;
		m_z += vec.m_z;
	}

	void float3::operator -= (float3 const& vec)
	{
		m_x -= vec.m_x;
		m_y -= vec.m_y;
		m_z -= vec.m_z;
	}

	void float3::operator *= (float multiplier)
	{
		m_x *= multiplier;
		m_y *= multiplier;
		m_z *= multiplier;
	}

	void float3::operator /= (float divisor)
	{
		m_x *= divisor;
		m_y *= divisor;
		m_z *= divisor;
	}

	void float3::operator = (float2 const& vec)
	{
		m_x = vec.m_x;
		m_y = vec.m_y;
		m_z = 0.0f;
	}

	void float3::operator = (float3 const& vec)
	{
		m_x = vec.m_x;
		m_y = vec.m_y;
		m_z = vec.m_z;
	}


	bool operator == (float3 const& a, float3 const& b)
	{
		return (a.m_x == b.m_x) && (a.m_y == b.m_y) && (a.m_z == b.m_z);
	}

	bool operator != (float3 const& a, float3 const& b)
	{
		return (a.m_x != b.m_x) || (a.m_y != b.m_y) || (a.m_z != b.m_z);
	}

	float3 operator + (float3 const& a, float3 const& b)
	{
		return float3((a.m_x + b.m_x), (a.m_y + b.m_y), (a.m_z + b.m_z));
	}

	float3 operator - (float3 const& a, float3 const& b)
	{
		return float3((a.m_x - b.m_x), (a.m_y - b.m_y), (a.m_z - b.m_z));
	}

	float3 operator * (float3 const& a, float multiplier)
	{
		return float3((a.m_x * multiplier), (a.m_y * multiplier), (a.m_z * multiplier));
	}

	float3 operator / (float3 const& a, float divisor)
	{
		return float3((a.m_x / divisor), (a.m_y / divisor), (a.m_z / divisor));
	}
}
