
#include "float2.h"
#include "float3.h"
#include "float4.h"
#include "Engine/Helpers/Asserts.hpp"
#include <float.h>
#include <math.h>


namespace JFF
{
	const float2 float2::INVALID = float2(FLT_MAX, FLT_MAX);

	const float2 float2::ZERO = float2(0.0f, 0.0f);
	const float2 float2::ONE = float2(1.0f, 1.0f);
	const float2 float2::RIGHT = float2(1.0f, 0.0f);
	const float2 float2::UP = float2(0.0f, 1.0f);


	float2::float2(float x, float y)
		: m_x(x)
		, m_y(y)
	{

	}


	void float2::Set(float x, float y)
	{
		m_x = x;
		m_y = y;
	}

	float3 float2::xyz() const
	{
		return float3(m_x, m_y, 0.0f);
	}

	float float2::DotProduct(float2 const& vec) const
	{
		return (m_x * vec.m_x) + (m_y * vec.m_y);
	}

	float float2::Length() const
	{
		return sqrtf(SqrdLength());
	}

	float float2::SqrdLength() const
	{
		return DotProduct(*this);
	}

	float2 float2::Rotate(float angle) const
	{
		const float sin = sinf(angle);
		const float cos = cosf(angle);

		return float2((m_x * cos) - (m_y * sin), (m_y * cos) + (m_x * sin));
	}

	float2 float2::Rotate90() const
	{
		return float2(-m_y, m_x);
	}

	float2 float2::Rotate180() const
	{
		return float2(-m_x, -m_y);
	}

	float2 float2::Rotate270() const
	{
		return float2(m_y, -m_x);
	}

	float2 float2::Normalize() const
	{
		const float lenght = Length();
		Assert(lenght > 0.0f, "Trying to normalize a vector of size 0");
		return (*this / lenght);
	}


	float2 float2::operator-()
	{
		return float2(-m_x, -m_y);
	}

	void float2::operator += (float2 const& vec)
	{
		m_x += vec.m_x;
		m_y += vec.m_y;
	}

	void float2::operator -= (float2 const& vec)
	{
		m_x -= vec.m_x;
		m_y -= vec.m_y;
	}

	void float2::operator *= (float multiplier)
	{
		m_x *= multiplier;
		m_y *= multiplier;
	}

	void float2::operator /= (float divisor)
	{
		m_x *= divisor;
		m_y *= divisor;
	}

	void float2::operator = (float x)
	{
		m_x = x;
		m_y = 0.0f;
	}

	void float2::operator = (float2 const& vec)
	{
		m_x = vec.m_x;
		m_y = vec.m_y;
	}


	bool operator == (float2 const& a, float2 const& b)
	{
		return (a.m_x == b.m_x) && (a.m_y == b.m_y);
	}

	bool operator != (float2 const& a, float2 const& b)
	{
		return (a.m_x != b.m_x) || (a.m_y != b.m_y);
	}

	float2 operator + (float2 const& a, float2 const& b)
	{
		return float2((a.m_x + b.m_x), (a.m_y + b.m_y));
	}

	float2 operator - (float2 const& a, float2 const& b)
	{
		return float2((a.m_x - b.m_x), (a.m_y - b.m_y));
	}

	float2 operator * (float2 const& a, float multiplier)
	{
		return float2((a.m_x * multiplier), (a.m_y * multiplier));
	}

	float2 operator / (float2 const& a, float divisor)
	{
		return float2((a.m_x / divisor), (a.m_y / divisor));
	}
}
