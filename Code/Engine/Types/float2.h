
#pragma once

#include <glm.hpp>


namespace JFF
{
	struct float3;
	struct float4;

	struct float2
	{
		friend struct float3;
		friend struct float4;

	public:

#pragma region Const statics

		const static float2 INVALID;

		const static float2 ZERO;
		const static float2 ONE;
		const static float2 RIGHT;
		const static float2 UP;

#pragma endregion


		float2(float x = 0.0f, float y = 0.0f);
		~float2() = default;

		void Set(float x, float y = 0.0f);

		inline float& x() { return m_x; }
		inline float& y() { return m_y; }
		inline float const& x() const { return m_x; }
		inline float const& y() const { return m_y; }
		float3 xyz() const;

		float DotProduct(float2 const& vec) const;
		float Length() const;
		float SqrdLength() const;
		float2 Rotate(float angle) const;
		float2 Rotate90() const;
		float2 Rotate180() const;
		float2 Rotate270() const;
		float2 Normalize() const;

		float2 operator - ();
		void operator += (float2 const& vec);
		void operator -= (float2 const& vec);
		void operator *= (float multiplier);
		void operator /= (float divisor);
		void operator = (float x);
		void operator = (float2 const& vec);

		operator glm::vec2() const { return glm::vec2(m_x, m_y); }

	private:

		float m_x;
		float m_y;


	public:

		friend bool operator == (float2 const& a, float2 const& b);
		friend bool operator != (float2 const& a, float2 const& b);

		friend float2 operator + (float2 const& a, float2 const& b);
		friend float2 operator - (float2 const& a, float2 const& b);
		friend float2 operator * (float2 const& a, float multiplier);
		friend float2 operator / (float2 const& a, float divisor);
	};
}
