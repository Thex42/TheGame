
#pragma once

#include "float2.h"
#include <glm.hpp>


namespace JFF
{
	struct float3;

	struct float4
	{
	public:

#pragma region Const statics

		const static float4 INVALID;

		const static float4 ZERO;
		const static float4 ONE;

#pragma endregion


		float4(float x = 0.0f, float y = 0.0f, float w = 0.0f, float z = 0.0f);
		float4(float x, float y, float2 const& zw);
		float4(float2 const& xy, float w = 0.0f, float z = 0.0f);
		float4(float2 const& xy, float2 const& zw);
		float4(float3 const& xy, float w = 0.0f);
		~float4() = default;

		inline float x() const { return m_xy.m_x; }
		inline float y() const { return m_xy.m_y; }
		inline float w() const { return m_zw.m_x; }
		inline float z() const { return m_zw.m_y; }
		inline float2 const& xy() const { return m_xy; }
		inline float2 const& wz() const { return m_zw; }
		float3 xyz() const;

		float4 operator - ();
		void operator += (float4 const& vec);
		void operator -= (float4 const& vec);
		void operator *= (float multiplier);
		void operator /= (float divisor);
		void operator = (float x);
		void operator = (float2 const& vec);
		void operator = (float3 const& vec);
		void operator = (float4 const& vec);

		operator glm::vec4() { return glm::vec4(m_xy.x(), m_xy.y(), m_zw.x(), m_zw.y()); }
		operator const glm::vec4() const { return glm::vec4(m_xy.x(), m_xy.y(), m_zw.x(), m_zw.y()); }

	private:

		float2 m_xy;
		float2 m_zw;


	public:

		friend bool operator == (float4 const& a, float4 const& b);
		friend bool operator != (float4 const& a, float4 const& b);

		friend float4 operator + (float4 const& a, float4 const& b);
		friend float4 operator - (float4 const& a, float4 const& b);
		friend float4 operator * (float4 const& a, const float multiplier);
		friend float4 operator / (float4 const& a, const float divisor);
	};
}
