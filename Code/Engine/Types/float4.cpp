
#include "float3.h"
#include "float4.h"
#include <float.h>


namespace JFF
{
	const float4 float4::INVALID = float4(FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX);

	const float4 float4::ZERO = float4(0.0f, 0.0f, 0.0f, 0.0f);
	const float4 float4::ONE = float4(1.0f, 1.0f, 1.0f, 1.0f);


	float4::float4(float x, float y, float w, float z)
		: m_xy(x, y)
		, m_zw(w, z)
	{

	}

	float4::float4(float x, float y, float2 const& zw)
		: m_xy(x, y)
		, m_zw(zw)
	{

	}

	float4::float4(float2 const& xy, float w, float z)
		: m_xy(xy)
		, m_zw(w, z)
	{

	}

	float4::float4(float2 const& xy, const float2& zw)
		: m_xy(xy)
		, m_zw(zw)
	{

	}

	float4::float4(float3 const& xyz, float w)
		: m_xy(xyz.xy())
		, m_zw(xyz.z(), w)
	{

	}


	float3 float4::xyz() const
	{
		return float3(m_xy, m_zw.x());
	}

	float4 float4::operator-()
	{
		return float4(-m_xy, -m_zw);
	}

	void float4::operator += (float4 const& vec)
	{
		m_xy += vec.m_xy;
		m_zw += vec.m_zw;
	}

	void float4::operator -= (float4 const& vec)
	{
		m_xy -= vec.m_xy;
		m_zw -= vec.m_zw;
	}

	void float4::operator *= (float multiplier)
	{
		m_xy *= multiplier;
		m_zw *= multiplier;
	}

	void float4::operator /= (float divisor)
	{
		m_xy /= divisor;
		m_zw /= divisor;
	}

	void float4::operator = (float x)
	{
		m_xy = x;
		m_zw = 0.0f;
	}

	void float4::operator = (float2 const& vec)
	{
		m_xy = vec;
		m_zw = 0.0f;
	}

	void float4::operator = (float3 const& vec)
	{
		m_xy = vec.xy();
		m_zw = vec.m_z;
	}

	void float4::operator = (float4 const& vec)
	{
		m_xy = vec.m_xy;
		m_zw = vec.m_zw;
	}


	bool operator == (float4 const& a, float4 const& b)
	{
		return ((a.m_xy == b.m_xy) && (a.m_zw == b.m_zw));
	}

	bool operator != (float4 const& a, float4 const& b)
	{
		return ((a.m_xy != b.m_xy) || (a.m_zw != b.m_zw));
	}

	float4 operator + (float4 const& a, float4 const& b)
	{
		return float4((a.m_xy + b.m_xy), (a.m_zw + b.m_zw));
	}

	float4 operator - (float4 const& a, float4 const& b)
	{
		return float4((a.m_xy - b.m_xy), (a.m_zw - b.m_zw));
	}

	float4 operator * (float4 const& a, float multiplier)
	{
		return float4((a.m_xy * multiplier), (a.m_zw * multiplier));
	}

	float4 operator / (float4 const& a, float divisor)
	{
		return float4((a.m_xy / divisor), (a.m_zw / divisor));
	}
}
