
#pragma once

#include "float2.h"
#include <glm.hpp>


namespace JFF
{
	struct float4;

	struct float3
	{
		friend struct float4;

	public:

#pragma region Const statics

		const static float3 INVALID;

		const static float3 ZERO;
		const static float3 ONE;
		const static float3 FORWARD;
		const static float3 RIGHT;
		const static float3 UP;

#pragma endregion


		float3(float x = 0.0f, float y = 0.0f, float z = 0.0f);
		float3(float2 const& xy, float z);
		float3(glm::vec3 const& xyz);
		~float3() = default;

		void Set(float x, float y = 0.0f, float z = 0.0f);

		inline float& x() { return m_x; }
		inline float& y() { return m_y; }
		inline float& z() { return m_z; }
		inline float const& x() const { return m_x; }
		inline float const& y() const { return m_y; }
		inline float const& z() const { return m_z; }
		float2 xy() const { return float2(m_x, m_y); }

		float DotProduct(float3 const& vec) const;
		float Length() const;
		float SqrdLength() const;
		float3 Normalize() const;

		float3 operator - ();
		void operator += (float3 const& vec);
		void operator -= (float3 const& vec);
		void operator *= (float multiplier);
		void operator /= (float divisor);
		void operator = (const float2& vec);
		void operator = (float3 const& vec);

		operator glm::vec3() { return glm::vec3(m_x, m_y, m_z); }
		operator const glm::vec3() const { return glm::vec3(m_x, m_y, m_z); }

	private:

		float m_x;
		float m_y;
		float m_z;


	public:

		friend bool operator == (float3 const& a, float3 const& b);
		friend bool operator != (float3 const& a, float3 const& b);

		friend float3 operator + (float3 const& a, float3 const& b);
		friend float3 operator - (float3 const& a, float3 const& b);
		friend float3 operator * (float3 const& a, float multiplier);
		friend float3 operator / (float3 const& a, float divisor);
	};
}
