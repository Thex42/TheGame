
#version 330 core

#define DIRECTIONAL_LIGHTS_COUNT 2


// It was expressed that some drivers required this next line to function properly
precision highp float;


struct LightDirectional
{
	vec3 m_direction;
	vec3 m_color;
	float m_intensity;
};


in vec2 _UV;
in vec3 _Normal;

uniform sampler2D u_MainTex;
uniform LightDirectional u_LightsDirectional[DIRECTIONAL_LIGHTS_COUNT];

out vec4 _FragColor;


// Functions prototypes
vec3 UseDirectionalLight(vec3 tFragColor, LightDirectional light, vec3 normal);


void main(void)
{
	vec3 normal = normalize(_Normal);

	vec3 tFragColor = texture(u_MainTex, _UV).xyz;
	vec3 tLitResult = vec3(0.0f);

	for(int i = 0; i < DIRECTIONAL_LIGHTS_COUNT; ++i)
		tLitResult += UseDirectionalLight(tFragColor, u_LightsDirectional[i], normal);

	_FragColor = vec4(tLitResult, 1.0f);
}

vec3 UseDirectionalLight(vec3 tFragColor, LightDirectional tLight, vec3 normal)
{
	float fDiffuse = max(dot(normal, -tLight.m_direction), 0.0f) * tLight.m_intensity;
	return (tFragColor * tLight.m_color) * fDiffuse;
}
