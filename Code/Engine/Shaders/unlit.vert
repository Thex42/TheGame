
#version 330 core

layout(location = 0) in vec3 in_tVertexPos_ModelSpace;
layout(location = 1) in vec2 in_tUV;
layout(location = 2) in vec3 in_tNormal;

uniform mat4 u_MVP;
uniform mat4 u_MP;

out vec2 _UV;
out vec3 _Normal;


void main(void)
{
	gl_Position = u_MVP * vec4(in_tVertexPos_ModelSpace, 1.0f);
	_UV = in_tUV;

	//_FragPos = (u_M * vec4(in_tVertexPos_ModelSpace, 1.0f)).xyz;
	_Normal = (u_MP * vec4(in_tNormal, 0.0f)).xyz;
}
