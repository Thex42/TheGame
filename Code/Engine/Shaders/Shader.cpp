
#include "Shader.h"
#include "Engine/Types/float3.h"
#include "Engine/Types/float4.h"
#include <gtc/type_ptr.hpp>
#include <sstream>


namespace JFF
{
	void JFFShader::UseProgram() const
	{
		glUseProgram(m_shaderProgramID);
	}

	void JFFShader::SetMVP(glm::mat4 const& modelMatrix, glm::mat4 const& viewMatrix, glm::mat4 const& projectionMatrix) const
	{
		const glm::mat4 mvpMatrix = modelMatrix * viewMatrix * projectionMatrix;
		const glm::mat4 mpMatrix = modelMatrix * projectionMatrix;

		glUniformMatrix4fv(m_mvpMatrixID, 1, GL_FALSE, glm::value_ptr(mvpMatrix));
		glUniformMatrix4fv(m_mpMatrixID, 1, GL_FALSE, glm::value_ptr(mpMatrix));
	}

	void JFFShader::SetFloat(float value, std::string const & valueName) const
	{
		int valueLocation = glGetUniformLocation(m_shaderProgramID, valueName.c_str());
		glUniform1f(valueLocation, value);
	}

	void JFFShader::SetVec3(glm::vec3 const& vector, std::string const& valueName) const
	{
		int valueLocation = glGetUniformLocation(m_shaderProgramID, valueName.c_str());
		glUniform3f(valueLocation, vector.x, vector.y, vector.z);
	}

	void JFFShader::SetVec4(glm::vec4 const& vector, std::string const& valueName) const
	{
		int valueLocation = glGetUniformLocation(m_shaderProgramID, valueName.c_str());
		glUniform4f(valueLocation, vector.x, vector.y, vector.z, vector.w);
	}

	void JFFShader::CleanUp()
	{
		/* Cleanup all the things we bound and allocated */
		glUseProgram(0);

		for (auto const& shader : m_shaders)
			glDetachShader(m_shaderProgramID, shader);

		glDeleteProgram(m_shaderProgramID);

		for (auto const& shader : m_shaders)
			glDeleteShader(shader);
	}

	std::string JFFShader::ReadFile(std::string const& fileName) const
	{
		const std::ifstream tFileStream(R"(Code\Engine\Shaders\)" + fileName);

		std::stringstream buffer;
		buffer << tFileStream.rdbuf();

		return buffer.str();
	}

	bool JFFShader::Init(std::string const& vertFileName, std::string const& fragFileName, std::string const& geomFileName /*= ""*/)
	{
		m_shaderProgramID = glCreateProgram();

		glBindAttribLocation(m_shaderProgramID, 0, "in_Position");
		glBindAttribLocation(m_shaderProgramID, 1, "in_Color");

		if (!LoadShader(vertFileName, GL_VERTEX_SHADER))
			return false;
		if (!geomFileName.empty() && !LoadShader(geomFileName, GL_GEOMETRY_SHADER))
			return false;
		if (!LoadShader(fragFileName, GL_FRAGMENT_SHADER))
			return false;
		if (!LinkShaders())
			return false;

		m_mvpMatrixID = glGetUniformLocation(m_shaderProgramID, "u_MVP");
		m_mpMatrixID = glGetUniformLocation(m_shaderProgramID, "u_MP");

		return true;
	}

	bool JFFShader::LoadShader(std::string const& filename, GLenum shaderType)
	{
		const std::string fileContent = ReadFile(filename);

		auto* shaderContent = const_cast<char*>(fileContent.c_str());
		const auto size = int32_t(fileContent.length());

		const GLuint shaderID = glCreateShader(shaderType);
		glShaderSource(shaderID, 1, &shaderContent, &size);
		glCompileShader(shaderID);

		int didCompiled = 0;
		glGetShaderiv(shaderID, GL_COMPILE_STATUS, &didCompiled);

		if (didCompiled == 0)
		{
			PrintShaderCompilationErrorInfo(shaderID, shaderType);
			return false;
		}

		glAttachShader(m_shaderProgramID, shaderID);

		m_shaders.push_back(shaderID);

		return true;
	}

	bool JFFShader::LinkShaders()
	{
		// Link. At this point, our shaders will be inspected/optized and the binary code generated
		// The binary code will then be uploaded to the GPU
		glLinkProgram(m_shaderProgramID);

		// Verify that the linking succeeded
		int didLink;
		glGetProgramiv(m_shaderProgramID, GL_LINK_STATUS, (int *)&didLink);

		if (didLink == 0)
			PrintShaderLinkingError(m_shaderProgramID);

		return didLink != 0;
	}

	std::string JFFShader::GetShaderPrintName(GLenum shaderType) const
	{
		std::string name = "NoShaderName";

		switch (shaderType)
		{
		case GL_VERTEX_SHADER: name = "Vertex"; break;
		case GL_TESS_CONTROL_SHADER: name = "Tess Control"; break;
		case GL_TESS_EVALUATION_SHADER: name = "Tess Evaluation"; break;
		case GL_GEOMETRY_SHADER: name = "Geometry"; break;
		case GL_FRAGMENT_SHADER: name = "Fragment"; break;
		}

		return name;
	}

	void JFFShader::PrintShaderLinkingError(int32_t shaderId) const
	{
		std::cout << "=======================================\n";
		std::cout << "Shader linking failed:\n";

		int maxLength;
		glGetProgramiv(shaderId, GL_INFO_LOG_LENGTH, &maxLength);

		std::cout << "Info Length : " << maxLength << '\n';

		auto* shaderInfoLog = new char[maxLength];
		glGetProgramInfoLog(m_shaderProgramID, maxLength, &maxLength, shaderInfoLog);

		std::cout << "Linker error message : " << shaderInfoLog << '\n';
		delete shaderInfoLog;
	}

	void JFFShader::PrintShaderCompilationErrorInfo(int32_t shaderId, GLenum shaderType) const
	{
		std::cout << "=======================================\n";
		std::cout << GetShaderPrintName(shaderType) << " shader compilation failed:\n";

		// Find length of shader info log
		int maxLength;
		glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &maxLength);

		// Get shader info log
		auto* shaderInfoLog = new char[maxLength];
		glGetShaderInfoLog(shaderId, maxLength, &maxLength, shaderInfoLog);

		// Print shader info log
		std::cout << "\tError info : " << shaderInfoLog << '\n';

		std::cout << "=======================================\n\n";
		delete shaderInfoLog;
	}


	std::map<std::string /*shaderName*/, JFFShader const*> JFFShadersHolder::s_loadedShadersByName;

	/*static*/ JFFShader const* JFFShadersHolder::GetOrCreateShader(std::string const& shaderName, bool hasGeomShader /*= false*/)
	{
		if (!HasShader(shaderName))
			s_loadedShadersByName[shaderName] = new JFFShader(shaderName + ".vert", shaderName + ".frag", (hasGeomShader ? shaderName + ".geom" : ""));

		return s_loadedShadersByName[shaderName];
	}
}
