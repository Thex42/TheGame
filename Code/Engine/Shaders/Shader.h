
#pragma once

#include <gl/glew.h>
#include <glm.hpp>
#include <SDL.h>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <vector>


namespace JFF
{
	class JFFShadersHolder;
	struct float3;
	struct float4;

	class JFFShader
	{
		friend class JFFShadersHolder;

	public:

		~JFFShader()
		{
			CleanUp();
		}

		void UseProgram() const;
		void SetMVP(glm::mat4 const& modelMatrix, glm::mat4 const& viewMatrix, glm::mat4 const& projectionMatrix) const;
		void SetFloat(float value, std::string const& valueName) const;
		void SetVec3(glm::vec3 const& vector, std::string const& valueName) const;
		void SetVec4(glm::vec4 const& vector, std::string const& valueName) const;

		void CleanUp();


	private:

		JFFShader(std::string const& vertFileName, std::string const& fragFileName, std::string const& geomFileName = "")
		{
			Init(vertFileName, fragFileName, geomFileName);
		}


		std::string ReadFile(std::string const& fileName) const;

		bool Init(std::string const& vertFileName, std::string const& fragFileName, std::string const& geomFileName = "");
		bool LoadShader(std::string const& filename, GLenum shaderType);
		bool LinkShaders();

		std::string GetShaderPrintName(GLenum shaderType) const;
		void PrintShaderLinkingError(int32_t shaderId) const;
		void PrintShaderCompilationErrorInfo(int32_t shaderId, GLenum shaderType) const;


		std::vector<GLuint> m_shaders;
		GLuint m_shaderProgramID;

		GLuint m_mvpMatrixID;
		GLuint m_mpMatrixID;
	};


	/*static*/ class JFFShadersHolder
	{
	public:

		static JFFShader const* GetOrCreateShader(std::string const& shaderName, bool hasGeomShader = false);
		inline static JFFShader const* GetShader(std::string const& shaderName) { return s_loadedShadersByName[shaderName]; }
		inline static bool HasShader(std::string const& shaderName) { return s_loadedShadersByName.find(shaderName) != s_loadedShadersByName.end(); }


	private:

		JFFShadersHolder() = default;	// Prevent instanciation


		static std::map<std::string /*shaderName*/, JFFShader const*> s_loadedShadersByName;
	};
}
