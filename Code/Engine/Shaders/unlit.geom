
#version 330

layout(triangles) in;
layout(triangle_strip, max_vertices = 100) out;

in vec4 pVertColor[]; // Output from vertex shader for each vertex

out vec4 _Color; // Output to fragment shader

void MakeEar(vec4 v);
void MakeBasicTriangle();
void MakeSplitTriangle(float fDist);
void MakeTriangle(vec4 tV1, vec4 tV2, vec4 tV3);

vec4 tCorner1 = gl_in[0].gl_Position;
vec4 tCorner2 = gl_in[1].gl_Position;
vec4 tCenter = gl_in[2].gl_Position;

float fExplodeDist = 0.66f;


void main()
{
	MakeSplitTriangle(fExplodeDist);
	MakeBasicTriangle();
	MakeEar(tCorner1);
	MakeEar(tCorner2);
}

void MakeSplitTriangle(float fDist)
{
	// Find middle of the two outer corners of the triangle
	vec4 tDiff = (tCorner1 - tCorner2) * fDist;

	// Generate an offset vector
	vec4 tOffset = vec4(0.0f);

	// Flip in order to get a normal
	// This is the direction the triangles will move
	tOffset.x = tDiff.y;
	tOffset.y = tDiff.x;

	tCorner1 += tOffset;
	tCorner2 += tOffset;
	tCenter += tOffset;

	MakeTriangle(tCenter, tCorner1, tCorner2);
}

void MakeBasicTriangle()
{
	vec4 tV1 = gl_in[0].gl_Position;
	vec4 tV2 = gl_in[1].gl_Position;
	vec4 tV3 = gl_in[2].gl_Position;

	MakeTriangle(tV1, tV2, tV3);
}

void MakeEar(vec4 v)
{
	vec4 tV1 = v + vec4(0.0f, 1.0f * v.y, 0.0f, 0.0f);
	vec4 tV2 = v + vec4(1.0f * v.x, 0.0f, 0.0f, 0.0f);
	vec4 tV3 = v + vec4(0.0f, 0.0f, 0.0f, 0.0f);

	MakeTriangle(tV1, tV2, tV3);
}

void MakeTriangle(vec4 tV1, vec4 tV2, vec4 tV3)
{
	gl_Position = tV1;
	_Color = pVertColor[0];
	EmitVertex();

	gl_Position = tV2;
	_Color = pVertColor[1];
	EmitVertex();

	gl_Position = tV3;
	_Color = pVertColor[2];
	EmitVertex();

	EndPrimitive();
}
